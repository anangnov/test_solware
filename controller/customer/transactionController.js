"use-strict";

const models = require("../../models");

const listTransaction = async (req, res) => {
  let data = await models.transaction.findAll({
    limit: parseInt(req.query.limit) || null,
    offset: parseInt(req.query.offset) || null
  });

  let objectResponse = {
    error: false,
    message: data.length > 0 ? "Found." : "Not found.",
    data: data,
    count: data.length
  };

  return req.output(req, res, objectResponse, "info", 200);
};

module.exports = { listTransaction };
