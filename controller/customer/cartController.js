"use-strict";

const models = require("../../models");

const addToCart = async (req, res) => {
  let objectResponse = {};

  // Check item
  let item = await models.item.findOne({
    where: {
      id: req.body.item_id
    }
  });

  if (item == null) {
    objectResponse = {
      error: false,
      message: "Item not found."
    };

    return req.output(req, res, objectResponse, "info", 200);
  }
  // End check item

  // Check qty
  if (item.qty < req.body.total_item) {
    objectResponse = {
      error: false,
      message: "Qty not enough."
    };

    return req.output(req, res, objectResponse, "info", 200);
  }
  // End check qty

  // Check user
  let user = await models.user.findOne({
    where: {
      id: req.body.user_id,
      role_id: 2
    }
  });

  if (user == null) {
    objectResponse = {
      error: false,
      message: "User not found."
    };

    return req.output(req, res, objectResponse, "info", 200);
  }
  // End check user

  let price = req.body.total_item * item.price;
  await models.cart.create({
    item_id: req.body.item_id,
    user_id: req.body.user_id,
    price: price,
    total_item: req.body.total_item
  });

  objectResponse = {
    error: false,
    message: "Success add to cart.",
    data: req.body
  };

  return req.output(req, res, objectResponse, "info", 201);
};

const listCart = async (req, res) => {
  let data = await models.cart.findAll({
    limit: parseInt(req.query.limit) || null,
    offset: parseInt(req.query.offset) || null
  });

  let objectResponse = {
    error: false,
    message: data.length > 0 ? "Found." : "Not found.",
    data: data,
    count: data.length
  };

  return req.output(req, res, objectResponse, "info", 200);
};

module.exports = { addToCart, listCart };
