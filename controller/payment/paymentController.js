"use-strict";

const models = require("../../models");

const pay = async (req, res) => {
  let objectResponse = {};

  // Check cart transaction
  let cartTransaction = await models.cart.findOne({
    where: {
      id: req.body.transaction_id
    }
  });

  if (cartTransaction == null) {
    objectResponse = {
      error: false,
      message: "Cart transaction not found."
    };

    return req.output(req, res, objectResponse, "info", 200);
  }
  // End check cart transaction

  // Get item
  let item = await models.item.findOne({
    where: {
      id: cartTransaction.item_id
    }
  });
  // End get item

  // Get user
  let user = await models.user.findOne({
    where: {
      id: cartTransaction.user_id
    }
  });
  // End get item

  // Insert transaction
  await models.transaction.create({
    item_id: cartTransaction.item_id,
    user_id: cartTransaction.user_id,
    total_price: cartTransaction.price * cartTransaction.total_item,
    total_item: cartTransaction.total_item,
    status: "pay"
  });
  // End insert transaction

  // Update item
  await models.item.update(
    {
      qty: Number(item.qty) - Number(cartTransaction.total_item)
    },
    {
      where: {
        id: cartTransaction.item_id
      }
    }
  );
  // End update item

  objectResponse = {
    error: false,
    message: "Succes pay.",
    data: {
      item_name: item.name,
      email: user.email,
      firstname: user.firstname,
      lastname: user.lastname,
      item_id: cartTransaction.item,
      price: item.price,
      total_price: cartTransaction.total_price,
      total_item: cartTransaction.total_item
    }
  };

  return req.output(req, res, objectResponse, "info", 200);
};

module.exports = { pay };
