"use-strict";

const async = require("async");
const models = require("../../models");
const jwt = require("../../utils/jwt");
const bcrypt = require("bcrypt");
const saltRounds = 12;

const register = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [
          {
            name: "email",
            value: req.body.email
          },
          {
            name: "role_id",
            value: req.body.role_id
          },
          {
            name: "firstname",
            value: req.body.firstname
          },
          {
            name: "lastname",
            value: req.body.lastname
          },
          {
            name: "password",
            value: req.body.password
          },
          {
            name: "confirm_password",
            value: req.body.confirm_password
          }
        ];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return callback(err);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function checkEmail(data, callback) {
        models.user
          .findOne({
            where: {
              email: req.body.email
            }
          })
          .then((result, err) => {
            if (err) return callback(err);

            if (result !== null)
              return callback({
                error: true,
                message: "Email was registered.",
                data: {}
              });

            callback(null, data);
          });
      },
      function checkPassword(data, callback) {
        if (req.body.password !== req.body.confirm_password)
          return callback({
            error: true,
            message: "Your password and confirmation password do not match.",
            data: {}
          });

        callback(null, data);
      },
      function createData(data, callback) {
        bcrypt.genSalt(saltRounds, (err, salt) => {
          bcrypt.hash(req.body.password, salt, (err, hash) => {
            models.user
              .create({
                role_id: req.body.role_id,
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.email,
                gender: req.body.gender.toLowerCase(),
                phone: req.body.phone,
                password: hash
              })
              .then((result, err) => {
                if (err) return callback(err);

                let roleName = "";
                if (req.body.role_id == 1) roleName = "admin";
                if (req.body.role_id == 2) roleName = "customer";

                callback(null, {
                  error: false,
                  message: `Success register as ${roleName}.`,
                  data: {}
                });
              })
              .catch(err => {
                callback(null, err);
              });
          });
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

const login = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [
          {
            name: "email",
            value: req.body.email
          },
          {
            name: "password",
            value: req.body.password
          },
          {
            name: "role_id",
            value: req.body.role_id
          }
        ];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function checkUser(data, callback) {
        models.user
          .findOne({
            where: {
              email: req.body.email
            }
          })
          .then((result, err) => {
            if (err) return callback(err);

            if (result == null) {
              return callback({
                error: true,
                message: "Email not found.",
                data: {}
              });
            }

            let hashDB = result.dataValues.password;
            bcrypt.compare(req.body.password, hashDB, function(
              err,
              resultHash
            ) {
              if (resultHash) {
                models.user
                  .findOne({
                    where: {
                      email: req.body.email,
                      password: hashDB,
                      is_active: true,
                      role_id: req.body.role_id
                    },
                    attributes: [
                      "role_id",
                      "email",
                      "firstname",
                      "lastname",
                      "gender",
                      "phone",
                      "is_active"
                    ]
                  })
                  .then((resultFinal, err) => {
                    if (err) return callback(err);
                    if (resultFinal == null)
                      return callback({
                        error: true,
                        message: "User not found.",
                        data: {}
                      });

                    callback(null, resultFinal);
                  });
              } else {
                return callback({
                  error: true,
                  message: "Email or password is wrong.",
                  data: {}
                });
              }
            });
          });
      },
      function final(user, callback) {
        console.log(user);
        let body = {
          email: user.email,
          role_id: user.role_id
        };

        let token = jwt.generateAccessToken(body);
        let data = {};
        data.token = token;
        data.email = user.email;
        data.firstname = user.firstname;
        data.lastname = user.lastname;
        data.phone = user.phone;
        data.gender = user.gender;
        data.is_active = user.is_active;

        let roleName = "";
        if (req.body.role_id == 1) roleName = "admin";
        if (req.body.role_id == 2) roleName = "customer";

        callback(null, {
          error: false,
          message: `Success login as ${roleName}.`,
          data: data
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

module.exports = { register, login };
