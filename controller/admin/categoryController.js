"use-strict";

const models = require("../../models");

const listCategory = async (req, res) => {
  let data = await models.category.findAll(
    {
      limit: parseInt(req.query.limit) || null,
      offset: parseInt(req.query.offset) || null
    },
    {
      where: {
        role_id: 2
      }
    }
  );

  let objectResponse = {
    error: false,
    message: data.length > 0 ? "Found." : "Not found.",
    data: data,
    count: data.length
  };

  return req.output(req, res, objectResponse, "info", 200);
};

module.exports = { listCategory };
