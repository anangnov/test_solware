"use-strict";

const models = require("../../models");
const slug = require("slug");

const listItem = async (req, res) => {
  let data = await models.item.findAll({
    offset: parseInt(req.query.offset) || null,
    limit: parseInt(req.query.limit) || null
  });

  let objectResponse = {
    error: false,
    message: data.length > 0 ? "Found." : "Not found.",
    data: data,
    count: data.length
  };

  return req.output(req, res, objectResponse, "info", 200);
};

const detailItem = async (req, res) => {
  let data = await models.item.findOne({
    where: {
      id: req.params.id
    }
  });

  let objectResponse = {
    error: false,
    message: data !== null ? "Found." : "Not found.",
    data: data
  };

  return req.output(req, res, objectResponse, "info", 200);
};

const createItem = async (req, res) => {
  await models.item.create({
    name: req.body.name,
    slug_name: slug(req.body.name),
    description: req.body.description,
    price: req.body.price,
    qty: req.body.qty,
    category_id: req.body.category_id
  });

  let objectResponse = {
    error: false,
    message: "Created.",
    data: req.body
  };

  return req.output(req, res, objectResponse, "info", 201);
};

const updateItem = async (req, res) => {
  let objectResponse = {};
  let find = await models.item.findOne({
    where: {
      id: req.params.id
    }
  });

  if (find == null) {
    objectResponse = {
      error: false,
      message: "Item Not found."
    };

    return req.output(req, res, objectResponse, "info", 200);
  }

  await models.item.update(
    {
      name: req.body.name,
      slug_name: slug(req.body.name),
      description: req.body.description,
      price: req.body.price,
      qty: req.body.qty,
      category_id: req.body.category_id
    },
    {
      where: {
        id: req.params.id
      }
    }
  );

  objectResponse = {
    error: false,
    message: "Updated.",
    data: req.body
  };

  return req.output(req, res, objectResponse, "info", 200);
};

const deleteItem = async (req, res) => {
  let objectResponse = {};
  let find = await models.item.findOne({
    where: {
      id: req.params.id
    }
  });

  if (find == null) {
    objectResponse = {
      error: false,
      message: "Item Not found."
    };

    return req.output(req, res, objectResponse, "info", 200);
  }

  await models.item.destroy({
    where: {
      id: req.params.id
    }
  });

  objectResponse = {
    error: false,
    message: "Deleted.",
    data: req.body.data
  };

  return req.output(req, res, objectResponse, "info", 200);
};

module.exports = { listItem, detailItem, createItem, updateItem, deleteItem };
