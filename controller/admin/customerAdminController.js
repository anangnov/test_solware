"use-strict";

const models = require("../../models");

const listCustomer = async (req, res) => {
  let data = await models.user.findAll(
    {
      attributes: ["email", "firstname", "lastname", "gender", "phone"],
      limit: parseInt(req.query.limit) || null,
      offset: parseInt(req.query.offset) || null
    },
    {
      where: {
        role_id: 2
      }
    }
  );

  let objectResponse = {
    error: false,
    message: data.length > 0 ? "Found." : "Not found.",
    data: data,
    count: data.length
  };

  return req.output(req, res, objectResponse, "info", 200);
};

const detailCustomer = async (req, res) => {
  let data = await models.user.findOne({
    attributes: ["email", "firstname", "lastname", "gender", "phone"],
    where: {
      id: req.params.id,
      role_id: 2
    }
  });

  let objectResponse = {
    error: false,
    message: data !== null ? "Found." : "Not found.",
    data: data
  };

  return req.output(req, res, objectResponse, "info", 200);
};

module.exports = { listCustomer, detailCustomer };
