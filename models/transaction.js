"use strict";

module.exports = function(sequelize, DataTypes) {
  return sequelize.define(
    "transaction",
    {
      item_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      total_price: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      total_item: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      tableName: "transactions",
      timestamps: false
    }
  );
};
