"use strict";

module.exports = function(sequelize, DataTypes) {
  return sequelize.define(
    "cart",
    {
      item_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      price: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      total_item: {
        type: DataTypes.INTEGER,
        allowNull: false
      }
    },
    {
      tableName: "carts",
      timestamps: false
    }
  );
};
