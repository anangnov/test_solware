"use strict";

module.exports = function(sequelize, DataTypes) {
  return sequelize.define(
    "item",
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      slug_name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false
      },
      price: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      qty: {
        type: DataTypes.BOOLEAN,
        allowNull: false
      },
      category_id: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      tableName: "items",
      timestamps: false
    }
  );
};
