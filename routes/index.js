const express = require("express");
const router = express.Router();
const jwt = require("../utils/jwt");
const authToken = jwt.authenticateToken;
const authController = require("../controller/auth/authController");
const itemController = require("../controller/admin/itemController");
const customerAdminController = require("../controller/admin/customerAdminController");
const transactionAdminController = require("../controller/admin/transactionAdminController");
const categoryController = require("../controller/admin/categoryController");
const cartController = require("../controller/customer/cartController");
const transactionController = require("../controller/customer/transactionController");
const paymentController = require("../controller/payment/paymentController");

module.exports = app => {
  router.post("/register", authController.register);
  router.post("/login", authController.login);

  // Admin
  router.get("/admin/items", authToken, itemController.listItem);
  router.get("/admin/item/:id", authToken, itemController.detailItem);
  router.post("/admin/item", authToken, itemController.createItem);
  router.patch("/admin/item/:id", authToken, itemController.updateItem);
  router.delete("/admin/item/:id", authToken, itemController.deleteItem);

  router.get(
    "/admin/customer",
    authToken,
    customerAdminController.listCustomer
  );
  router.get(
    "/admin/customer/:id",
    authToken,
    customerAdminController.detailCustomer
  );

  router.get(
    "/admin/transactions",
    authToken,
    transactionAdminController.listTransaction
  );
  router.get(
    "/admin/transaction/:id",
    authToken,
    transactionAdminController.detailTransaction
  );

  router.get("/admin/category", authToken, categoryController.listCategory);
  // End Admin

  // Customer
  router.post("/cart", authToken, cartController.addToCart);
  router.get("/carts", authToken, cartController.listCart);

  router.get("/transactions", authToken, transactionController.listTransaction);
  router.post("/pay", authToken, paymentController.pay);
  // End Customer

  app.use("/api", router);
};
